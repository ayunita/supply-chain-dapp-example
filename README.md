# Supply Chain DApp
Owner can add items, move the items forward to supply chain, and gives buyer an item contract address to send money to. Once the item is paid, owner can trigger a delivery.

## Getting Started

### Prerequisites
npm: 6.14.4
node: 10.19.0
metamask

### Installing
Install dependencies
```
npm install
```

Populate .env
```
REACT_APP_RINKEBY_URI=<https://rinkeby.infura.io/KEY>
REACT_APP_WEBSOCKET_URI=<wss://rinkeby.infura.io/ws/KEY>
REACT_APP_CONTRACT_ITEM_MANAGER=0xD5FcacfD3F2b160f8ADA53b987e44b88cC146681
```

Run the client's development server
```
npm start
```