import React, { useState, useEffect } from "react";
import ReactTooltip from "react-tooltip";
import ItemManager from "./contracts/ItemManager.json";
import Item from "./contracts/Item.json";
import Web3 from "web3";
import getWeb3 from "./getWeb3";
import "./App.css";

const SUPPLY_CHAIN_STEPS = {
  0: "Created",
  1: "Paid",
  2: "Delivered",
};

const App = () => {
  // Initialize the state of App
  const [newItem, setNewItem] = useState({
    cost: 0,
    itemName: "Item 1",
  });
  const [itemList, setItemList] = useState([]);
  const [account, setAccount] = useState(null);
  const [instance, setInstance] = useState({
    itemManager: null,
  });
  const [loaded, setLoaded] = useState(false);
  const [listLoaded, setListLoaded] = useState(false);

  useEffect(() => {
    const loadWeb3 = async () => {
      try {
        // Get network provider and web3 instance.
        const _web3 = await getWeb3();
        // Use web3 to get the user's accounts.
        const _accounts = await _web3.eth.getAccounts();
        if (_accounts) setAccount(_accounts[0]);

        // Get the contract instance.
        const _itemManager = new _web3.eth.Contract(
          ItemManager.abi,
          process.env.REACT_APP_CONTRACT_ITEM_MANAGER
        );

        if (_itemManager) {
          setInstance({ itemManager: _itemManager });
        }

        setLoaded(true);
      } catch (error) {
        // Catch any errors for any of the above operations.
        alert(
          "Failed to load web3, accounts, or contract. Check console for details."
        );
        console.error(error);
      }
    };

    loadWeb3();
  }, []);

  useEffect(() => {
    // Update item list UI
    getItemList();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const listenToPaymentEvent = () => {
      // Web3 using websocket to listen emitted events
      const _web3 = new Web3(
        new Web3.providers.WebsocketProvider(
          process.env.REACT_APP_WEBSOCKET_URI
        )
      );

      const _itemManager = new _web3.eth.Contract(
        ItemManager.abi,
        process.env.REACT_APP_CONTRACT_ITEM_MANAGER
      );

      _itemManager.events.SupplyChainStep().on("data", async function (evt) {
        if (Number(evt.returnValues._step) === 1) {
          let item = await _itemManager.methods
            .items(evt.returnValues._itemIndex)
            .call();
          alert(`Item ${item._identifier} was paid, deliver it now!`);
          getItemList(); // Update item list UI
        }
      });
    };

    // Listen to payment event.
    // Whenever buyer sends payment to Item contract, it will alert the owner that payment has been made.
    listenToPaymentEvent();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  // BUG: Need to reinit the web3 & instance for getItemList called inside event listener
  const getItemList = async () => {
    setListLoaded(false);

    const _web3 = new Web3(
      new Web3.providers.HttpProvider(process.env.REACT_APP_RINKEBY_URI)
    );
    const _itemManager = new _web3.eth.Contract(
      ItemManager.abi,
      process.env.REACT_APP_CONTRACT_ITEM_MANAGER
    );

    let itemList = await _itemManager.methods.getAllItems().call();
    if (itemList) {
      let items = [];
      // itemList.pop(); // remove last element, which is address(0)
      for (let i = 0; i < itemList.length; i++) {
        if (itemList[i]._item === "0x0000000000000000000000000000000000000000")
          continue;
        let obj = {
          _item: itemList[i]._item,
          _identifier: itemList[i]._identifier,
          _step: itemList[i]._step,
        };

        const x = await new _web3.eth.Contract(Item.abi, itemList[i]._item);
        if (x) {
          await x.methods
            .priceInWei()
            .call()
            .then(
              (result) =>
                (obj = { ...obj, _price: Number(result) / 1000000000000000000 })
            );
          await x.methods
            .index()
            .call()
            .then((result) => (obj = { ...obj, _index: Number(result) }));
        }
        items.push(obj);
      }
      setItemList(items.reverse());
    }
    setListLoaded(true);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const { cost, itemName } = newItem;
    try {
      let result = await instance.itemManager.methods
        .createItem(itemName, cost)
        .send({
          from: account,
          gas: 1500000,
          gasPrice: "30000000000",
        });
      alert(
        `Send ${cost} Wei to ${result.events.SupplyChainStep.returnValues._address}`
      );
      getItemList(); // Update item list UI
    } catch (error) {
      console.log("Error: ", error);
    }
  };

  const handleSend = async (index) => {
    try {
      await instance.itemManager.methods.triggerDelivery(index).send({
        from: account,
        gas: 1500000,
        gasPrice: "30000000000",
      });
      alert(`Delivered!`);
      getItemList(); // Update item list UI
    } catch (error) {
      console.log("Error: ", error);
    }
  };

  return (
    <div className="App">
      <div className="container">
        {!loaded ? <div className="loading"> Loading Web3, accounts, and contract... </div> : null}
        <div className="header">
          <span className="logo" role="img" aria-label="truck">
            🚚
          </span>
          <span>
            <h1>Simply Payment/Supply Chain Example!</h1>
          </span>
        </div>
        <h2>New Item</h2>
        <form className="column" onSubmit={handleSubmit}>
          <div className="input-group">
            <span className="label">Cost (wei):</span>
            <input
              type="number"
              required
              name="cost"
              value={newItem.cost}
              onChange={(event) =>
                setNewItem({ ...newItem, cost: event.target.value })
              }
            />
          </div>
          <div className="input-group">
            <span className="label">Item Name:</span>{" "}
            <input
              type="text"
              required
              name="itemName"
              value={newItem.itemName}
              onChange={(event) =>
                setNewItem({ ...newItem, itemName: event.target.value })
              }
            />
          </div>
          <div className="input-group">
            <span className="label"></span>
            <button type="submit" className="button">
              <span data-tip data-for="create-hint">
                Add New Item
              </span>
              <ReactTooltip id="create-hint" type="info">
                <span>Only contract's owner can add new item!</span>
              </ReactTooltip>
            </button>
          </div>
        </form>
        <h2>Item list</h2>
        <div className="column">
          {!listLoaded ? <span className="loading">Refreshing the list...</span> : null}
          <table>
            <tbody>
              {itemList.map((item, index) => {
                return (
                  <tr key={index}>
                    <td>{item._identifier}</td>
                    <td>{item._price} ETH</td>
                    <td>
                      <span data-tip data-for="address-hint">
                        {item._item}
                      </span>
                      <ReactTooltip id="address-hint" type="info">
                        <span>Send ETH to this contract address</span>
                      </ReactTooltip>
                    </td>
                    <td>
                      <span
                        className={`tag ${
                          Number(item._step) === 0
                            ? "tag-created"
                            : Number(item._step) === 1
                            ? "tag-paid"
                            : Number(item._step) === 2
                            ? "tag-delivered"
                            : ""
                        }`}
                      >
                        {SUPPLY_CHAIN_STEPS[item._step]}
                      </span>
                    </td>
                    <td width="50px">
                      {Number(item._step) === 1 ? (
                        <button
                          type="button"
                          className="button-icon"
                          onClick={() => handleSend(item._index)}
                        >
                          <span
                            data-tip
                            data-for="send"
                            role="img"
                            aria-label="arrow"
                          >
                            ➡️
                          </span>
                          <ReactTooltip id="send" type="info">
                            <span>Only contract's owner can deliver!</span>
                          </ReactTooltip>
                        </button>
                      ) : null}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
      <div>
        Built on Rinkeby testnet | Visit me{" "}
        <a
          target="_blank"
          rel="noreferrer noopener"
          href="https://ayunita.xyz/"
        >
          here
        </a>
      </div>
    </div>
  );
};

export default App;
